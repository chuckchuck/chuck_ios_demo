//
//  AppDelegate.h
//  Chuck-demo
//
//  Created by Derek on 27/11/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

