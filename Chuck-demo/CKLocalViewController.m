//
//  CKLocalViewController.m
//  Chuck-demo
//
//  Created by Derek on 28/11/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import "CKLocalViewController.h"

@interface CKLocalViewController ()

@end

@implementation CKLocalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
