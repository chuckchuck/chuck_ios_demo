//
//  CKViewViewController.m
//  Chuck-demo
//
//  Created by Derek on 29/11/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import "CKViewViewController.h"
#import "SwipeView.h"
#import "CKViewCell.h"


@interface CKViewViewController ()
@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (strong, nonatomic) NSMutableArray *items;

@end

@implementation CKViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _items = [NSMutableArray array];
    for (int i=0; i<20; ++i) {
        [_items addObject:@(i)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    self.swipeView.dataSource = nil;
    self.swipeView.delegate = nil;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return [_items count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    if (view == nil) {
        view = [[NSBundle mainBundle] loadNibNamed:@"CKViewCell" owner:self options:nil][0];
    }
    
    CKViewCell *cell = (CKViewCell *)view;
    
    cell.image.backgroundColor = self.backgroundColor;
    
    return view;

}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}


@end
