//
//  CKViewViewController.h
//  Chuck-demo
//
//  Created by Derek on 29/11/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKViewViewController : UIViewController

@property(nonatomic, strong) UIColor* backgroundColor;

@end
