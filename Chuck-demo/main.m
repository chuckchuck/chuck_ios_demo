//
//  main.m
//  Chuck-demo
//
//  Created by Derek on 27/11/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
