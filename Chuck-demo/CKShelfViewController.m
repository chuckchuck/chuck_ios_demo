//
//  CKShelfViewController.m
//  Chuck-demo
//
//  Created by Derek on 3/12/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import "CKShelfViewController.h"
#import "SwipeView.h"
#import "CKShelfCell.h"
#import "CKViewViewController.h"


@interface CKShelfViewController () <SwipeViewDataSource, SwipeViewDelegate>
@property (weak, nonatomic) IBOutlet SwipeView *swipeView;
@property (strong, nonatomic) NSMutableArray *items;
@property (strong, nonatomic) UIColor *currentColor;

@end

@implementation CKShelfViewController

- (instancetype) init {
    self = [super init];
    if (self) {
        UINavigationItem *navigationItem = self.navigationItem;
        navigationItem.title = @"Shelf";
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _items = [NSMutableArray array];
    for (int i=0; i<20; ++i) {
        [_items addObject:@(i)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    self.swipeView.dataSource = nil;
    self.swipeView.delegate = nil;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInSwipeView:(SwipeView *)swipeView
{
    //return the total number of items in the carousel
    return [_items count];
}

- (UIView *)swipeView:(SwipeView *)swipeView viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    //    UILabel *label = nil;
    //
    //    //create new view if no view is available for recycling
    //    if (view == nil)
    //    {
    //        //don't do anything specific to the index within
    //        //this `if (view == nil) {...}` statement because the view will be
    //        //recycled and used with other index values later
    //        CGRect rect = CGRectMake(200, 200, 500, 500);
    //        view = [[UIView alloc] initWithFrame:rect];
    //        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //
    //        label = [[UILabel alloc] initWithFrame:view.bounds];
    //        label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //        label.backgroundColor = [UIColor clearColor];
    //        label.textAlignment = NSTextAlignmentCenter;
    //        label.font = [label.font fontWithSize:50];
    //        label.tag = 1;
    //        [view addSubview:label];
    //    }
    //    else
    //    {
    //        //get a reference to the label in the recycled view
    //        label = (UILabel *)[view viewWithTag:1];
    //    }
    //
    //    //set background color
    //    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    //    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    //    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    //    view.backgroundColor = [UIColor colorWithRed:red
    //                                           green:green
    //                                            blue:blue
    //                                           alpha:1.0];
    //
    //    //set item label
    //    //remember to always set any properties of your carousel item
    //    //views outside of the `if (view == nil) {...}` check otherwise
    //    //you'll get weird issues with carousel item content appearing
    //    //in the wrong place in the carousel
    //    label.text = [_items[index] stringValue];
    //
    //    return view;
    
    if (view == nil) {
        view = [[NSBundle mainBundle] loadNibNamed:@"CKShelfCell" owner:self options:nil][0];
    }
    
    CKShelfCell *cell = (CKShelfCell*)view;
    
    CGFloat red = arc4random() / (CGFloat)INT_MAX;
    CGFloat green = arc4random() / (CGFloat)INT_MAX;
    CGFloat blue = arc4random() / (CGFloat)INT_MAX;
    self.currentColor = [UIColor colorWithRed:red
                                    green:green
                                    blue:blue
                                    alpha:1.0];
    cell.cover.backgroundColor = self.currentColor;
    
    return view;
    
}

- (CGSize)swipeViewItemSize:(SwipeView *)swipeView
{
    return self.swipeView.bounds.size;
}


#pragma mark -
#pragma mark SwipeView Delegate

- (BOOL)swipeView:(SwipeView *)swipeView shouldSelectItemAtIndex:(NSInteger)index {
    return YES;
}

- (void)swipeView:(SwipeView *)swipeView didSelectItemAtIndex:(NSInteger)index {
    CKViewViewController *viewViewController = [[CKViewViewController alloc] init];
    viewViewController.backgroundColor = self.currentColor;
    
    [self.navigationController pushViewController:viewViewController animated:YES];
}

@end
