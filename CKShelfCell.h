//
//  CKShelfCell.h
//  Chuck-demo
//
//  Created by Derek on 3/12/15.
//  Copyright © 2015 chuck. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CKShelfCell : UIView

@property (weak, nonatomic) IBOutlet UIView *cover;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
